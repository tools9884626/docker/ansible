FROM python:3.11.9-alpine3.19

ENV ANSIBLE_CORE_VERSION=2.17.2

RUN mkdir /ansible && \
  apk add --no-cache git gcc musl-dev libffi-dev openssl-dev openssh-client bash sshpass && \
  pip install --no-cache-dir ansible ansible-lint && \
  pip install --no-cache-dir ansible-core==${ANSIBLE_CORE_VERSION}

WORKDIR /ansible

CMD [ "ansible", "--version" ]
